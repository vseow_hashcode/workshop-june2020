#!/bin/bash
_term() { 
  echo "Caught SIGTERM signal!" 
  /root/wienchain-cli stop
#   kill "$child" 2>/dev/null
}
_intm() { 
  echo "Caught SIGINT signal!" 
  /root/wienchain-cli stop
#   kill "$child" 2>/dev/null
}

trap _term SIGTERM
trap _intm SIGINT

## Start the wienchaind
sleep 15
/root/wienchaind -conf=/var/wienchaind/wienchain.conf -datadir=/var/wienchaind &

child=$!
wait "$child"

## To monitor debug log if failed.
# /usr/bin/tail -f /root/.wienchain/debug.log &
# child=$!
# wait "$child"