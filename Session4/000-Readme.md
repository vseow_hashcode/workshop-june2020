# Cramp Workshop Session 2
## Getting Started
#### Build Docker Image
```
cd build
docker build . -t session4
```

#### Run Docker Compose
```
docker-compose up
```

#### Exec into a Container
```
docker exec -it 2560be239260 bash
```
