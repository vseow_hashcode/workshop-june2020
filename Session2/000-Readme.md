# Cramp Workshop Session 2
## Docker Hub Login and Push
```
docker login
```


#### Start a Wordpress
```
docker run --name some-wordpress -p 8080:80 -d wordpress
```

#### Check Running Containers
```
docker ps
```

#### Start a stack
```
docker-compose up -d
```

#### Stop a stack
```
docker-compose down
```

