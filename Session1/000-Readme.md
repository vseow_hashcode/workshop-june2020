# Cramp Workshop Session 1
## Getting Started
#### Verify Installation
```
docker --version
```

#### Run Some Containers
Get started by running some containers.

```
docker run hello-world
```

```
docker run -it ubuntu bash
```

```
docker run -it --rm busybox
```
- `docker` The CLI.
- `run` The command to run a container.
- `-it` Interactive command.
- `--rm` Remove the container once stopped.

#### Cleaning Stopped Containers
```
docker container prune
```

- `docker` The CLI.
- `container` The command to manage containers.
- `prune` Sub command to prune stopped containers.

## My First Image
#### Build the docker file
```
docker build . -f .\001-FirstDockerFile
```

#### Run with Hash
The hash differs depending on your output from command above.
```
docker run --rm 792b9dd0eb15
```

#### Build and Tag

```
docker build . -f .\001-FirstDockerFile -t first-image
```
_Note: The tag name must be all lowercase._


```
docker run --rm first-image
```

#### Versioning
```
docker build . -f .\001-FirstDockerFile -t first-image:tag1
```


```
docker run --rm first-image:tag1
```