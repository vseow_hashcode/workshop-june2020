# Cramp Workshop Session 2
## Getting Started
#### Start a Python image
```
docker run -it python:3 bash
pip install fabric
apt-get update
apt-get install nano -y
nano fabfile.py
```

Copy paste the content from fabric.py into the file
hit `CTRL+X` , `Y`, `ENTER`, to save changes.
Type `cat fabric.py` to verify.
#### Build the fabric Image
```
docker build . -t fabric
```

#### Run the fabric Image with mapping
```
docker run -it --rm --mount type=bind,source="$(pwd)"/fabric,target=/fabric/ fabric
```

```
docker run -it --rm `
--mount type=bind,source="$(pwd)"/fabric,target=/fabric/ `
fabric
```