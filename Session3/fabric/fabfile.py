# Import Fabric's API module
from fabric import Connection, Config
from invoke import task, call, Responder

host = 'root@172.104.183.43'
passw = 'session3'

# Compile a list of hosts based on nodes.py
@task
def getDate(c):
    c = Connection(host, config=Config(), connect_kwargs={'password':passw})
    c.run('date')

@task
def installDocker(c):
    c = Connection(host, config=Config(), connect_kwargs={'password':passw})
    install_docker(c)
    install_docker_compose(c)

# Helper functions
def install_docker(c):
	c.run('yum install yum-utils device-mapper-persistent-data lvm2 -y')
	c.run('yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo')
	c.run('yum install docker-ce docker-ce-cli containerd.io -y')
	c.run('systemctl start docker')

def install_docker_compose(c):
	c.run('curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose')
	c.run('chmod +x /usr/local/bin/docker-compose')
	c.run('docker-compose --version')


@task
def deployFiles(c):
    c = Connection(host, config=Config(), connect_kwargs={'password':passw})
    c.run('mkdir -p /root/vseow')
    c.put('deploy/docker-compose.yaml', '/root/vseow/docker-compose.yaml')
    c.run('ls /root/vseow')

@task
def runApp(c):
    c = Connection(host, config=Config(), connect_kwargs={'password':passw})
    with c.cd ('/root/vseow') :
        c.run('docker-compose up -d')
   
@task
def stopApp(c):
    c = Connection(host, config=Config(), connect_kwargs={'password':passw})
    with c.cd ('/root/vseow') :
        c.run('docker-compose down')

